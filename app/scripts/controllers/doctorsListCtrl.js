/**
 * Created by mobstac on 21/03/15.
 */


var doctors = angular.module('terzettoWebsiteApp');

doctors.controller('DoctorsListCtrl', ['$scope', function($scope){


    $('.date').datepicker({
        todayHighlight: true,
        format: 'dd-mm-yyyy'
     });

    $('.slider').slider();


    var specialities = ['Allergist', 'Anesthesiologist', 'Cardiologist', 'Dermatologist','Dentist', 'Gastroenterologist',
        'Hematologist', 'Nephrologist', 'Neurologist', 'Obstetrician', 'Gynecologist', 'Pathologist',
        'Pediatrician', 'Radiation Onconlogist', 'Urologist'
    ];
    var substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            var matches, substrRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    // the typeahead jQuery plugin expects suggestions to a
                    // JavaScript object, refer to typeahead docs for more info
                    matches.push({ value: str });
                }
            });

            cb(matches);
        };
    };

    $('#speciality-dropdown-menu .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'specialities',
            displayKey: 'value',
            source: substringMatcher(specialities)
        });




}]);