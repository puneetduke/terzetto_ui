'use strict';

/**
 * @ngdoc function
 * @name terzettoWebsiteApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the terzettoWebsiteApp
 */
angular.module('terzettoWebsiteApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
