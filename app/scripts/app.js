'use strict';

/**
 * @ngdoc overview
 * @name terzettoWebsiteApp
 * @description
 * # terzettoWebsiteApp
 *
 * Main module of the application.
 */
angular
  .module('terzettoWebsiteApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
      })
      .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl'
        })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
        .when('/doctors-list',{
            templateUrl: 'views/doctorsList.html',
            controller: 'DoctorsListCtrl'
        })
      .otherwise({
        redirectTo: '/'
      });
  });
